# IDE WG Dev Funded Program Planning Council Top Issues

At the Eclipse Foundation, most open source projects progress using developers who are employed by members, self-employed, or volunteers. In some instances Working Groups may, in exceptional circumstances,  choose to allocate a portion of their budget to fund development efforts in support of the projects within their purview. Working Groups with applicable budgets are able to take advantage of an initiative to spend funds to  contract development efforts to advance the working group and/or its associated projects.

Such funding is over and above the more typical way projects and initiatives get advanced. Examples of such development efforts include project process enhancements or updates, addressing outstanding issues or other inhibitors preventing use or adoption of the project(s) in their current form, or addressing issues related to the working group hosting a version of its own project(s)  (e.g., the Open VSX initiative within Eclipse Cloud Development Tools working group), etc.  

In order to carry out such development efforts, the Eclipse Foundation will manage the development effort on behalf of the working group. The Foundation will, in turn, seek to engage with capable service providers to see that these development efforts get carried out. 

The Eclipse Foundation (EF) will engage with the Working Group’s Steering Committee, or their delegate if so designated (for example, the Planning Council in the Eclipse IDE Working Group) as the key stakeholders in the community to determine the highest priority development effort requests to be addressed, convert these priorities into actionable development tasks, and then engage with qualified resources to carry out these tasks.

The guiding principles of the process are:


To adhere to the principles of transparency and openness,
To complement the existing community development efforts,
To encourage a “multiplying effect” where community participation is amplified by this funding program’s undertakings,
To ensure the funds allocated fit within the overall working group program plan and budget. 

Governance:
Arranging for the implementation of development efforts and initiatives will be managed exclusively by the Eclipse Foundation on behalf of the  working group. The EF commits to being transparent with regard to with whom it has chosen to engage and a summary of the expenditures made on behalf of the working group. Having said this, the EF recognizes and supports that pricing for services is considered confidential by many vendors, and the EF will work to respect that confidentiality while still ensuring working group members are aware of the total value being delivered.  

The Foundation will designate an individual to manage the various tasks on its behalf (the “EF Rep”). It is expected that the Steering Committee or other committee will name one to three delegates to collaborate with the EF Rep.  The delegate(s) can come from within the Steering Committee or another working group body, or their designate if more appropriate. Together, the delegates and the EF Rep form a small ad hoc team (the “Team”).

Read More in the [Eclipse Foundation Working Group Management of Development Effort Guidelines](https://www.eclipse.org/org/workinggroups/wgfi_program.php).


